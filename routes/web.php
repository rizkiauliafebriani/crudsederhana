<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//proses generate app key
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

//add merk menggunakan form
$router->get('/add-post', 'PostController@addPost');

//proses tambah merk
$router->post('/add-post/proses', 'PostController@proses_post');

//menampilkan tabel
$router->get('/posts','PostController@getPosts');

//proses hapus merk
$router->get('/delete-post/{id}','PostController@deletePost');

//proses edit merk
$router->get('/update-post/update','PostController@updatePost');

//edit menggunakan form
$router->get('/edit-post/{id}','PostController@editForm');