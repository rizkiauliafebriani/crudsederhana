<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    //insert menggunakan form
    public function addPost()
    {
    	return view('add-post');
    }

    //proses insert
    public function proses_post(Request $request)
    {
    	$post = new Post();
    	$post->nama_merk = $request->nama_merk;
    	$post->save();
    	return "Merk Berhasil Ditambahkan";
    }

    //menampilkan semua data dari database
    public function getPosts()
    {
    	$posts = Post::all();
    	return view('post',['posts'=>$posts]); // nama blase , nama variabel 
    }

    //proses delete berdasarkan id merk
    public function deletePost($id)
    {
    	Post::where('id_merk',$id)->delete();
    	return "Merk berhasil dihapus";
    }

    //proses update
    public function updatePost(Request $request)
    {
    	$post = Post::where('id_merk',$request->id_merk)->first();
    	$post->nama_merk = $request->nama_merk;
    	$post->save();
    	return "Nama Merk berhasil di edit";
    }

    //update menggunakan form
    public function editForm($id)
    {
    	$post = Post::where('id_merk',$id)->first();
    	return view('edit-post',['post'=>$post]); //nama blade
    }
}
