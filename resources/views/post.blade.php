<!DOCTYPE html>
<html>
<head>
	<title>All Merk</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 align="center">Semua Merk</h1>
					<br>
					<button><a href="/add-post">Tambah Merk</a></button>
					<br>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Id Merk</th>
								<th>Nama Merk</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $post)
								<tr>
									<td>{{$post->id_merk}}</td>
									<td>{{$post->nama_merk}}</td>
									<td>
										<button><a href="/edit-post/{{$post->id_merk}}">Edit</a></button> |
										<button><a href="/delete-post/{{$post->id_merk}}">Hapus</a></button>	
									</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>